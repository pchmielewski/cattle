﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Cattle.Domain.Entities;

namespace Cattle.Domain
{
    public class CattlesData:DbContext
    {
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Research> Researches { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Cattle.Domain.Entities.Cattle> Cattles { get; set; }

    }
}

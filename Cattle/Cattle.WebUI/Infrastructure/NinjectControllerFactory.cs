﻿using System.Web.Routing;
using Ninject;
using System.Web.Mvc;
using System;

namespace Cattle.WebUI.Infrastructure
{
    public class NinjectControllerFactory: DefaultControllerFactory
    {
        public IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }
        private void AddBindings()
        {
 
        }
    }
}
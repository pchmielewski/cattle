﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cattle.Domain.Enums
{
    public enum SexType
    {
        female =1,
        male =2,
        unknown =3
    }
}

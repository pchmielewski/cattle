﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Cattle.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Cattle.Domain.Entities
{
    [Table("Researches")]
    public class Research
    {
        [Key]
        public Guid Guid { get; set; }
        public string Doctor { get; set; }
        public DateTime ResearchDate { get; set; }
        public string Description { get; set; }
    }
}

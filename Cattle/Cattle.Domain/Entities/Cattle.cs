﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Cattle.Domain.Enums;

namespace Cattle.Domain.Entities
{
    [Table("Cattles")]
    public class Cattle
    {
        [Key]
        public Guid Guid { get; set; }
        public string Name { get; set; }
        ///.......
    }
}

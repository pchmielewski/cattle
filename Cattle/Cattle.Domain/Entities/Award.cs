﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Cattle.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Cattle.Domain.Entities
{
    [Table("Awards")]
    public class Award
    {
        [Key]
        public Guid Guid { get; set; }
        public string AwardName { get; set; }
        public int Place { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}

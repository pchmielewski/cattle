﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Cattle.Domain.Enums;

namespace Cattle.Domain.Entities
{
    [Table("Animals")]
    public class Animal
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }

        public string AnimalType { get; set; }
        public string Name { get; set; }
        public string Race { get; set; }
        public double Performance { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Weight { get; set; }
        public SexType Sex { get; set; } 
        public string EDI { get; set; }
        public string OrginsCountry { get; set; }
        [Column(TypeName="image")]
        public byte[] MatrixCode { get; set; }
        [ForeignKey("Animal")]
        public Guid MotherId { get; set; }
        [ForeignKey("Animal")]
        public Guid FatherId { get; set; }
        [ForeignKey("Cattle")]
        public Guid CattleId { get; set; }
        [ForeignKey("Research")]
        public Guid ResearchId { get; set; }
        [ForeignKey("Award")]
        public Guid AwardId { get; set; }

        public virtual Animal Mother { get; set; }
        public virtual Animal Father { get; set; }
        public virtual Cattle Cattle { get; set; }
        public virtual Research Research { get; set; }
        public virtual Award Award { get; set; }
    }
}
